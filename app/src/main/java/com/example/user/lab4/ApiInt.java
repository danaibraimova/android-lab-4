package com.example.user.lab4;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInt {

    @GET("posts")
    Call<List<News>> getPosts();


    @POST("posts")
    Call<News> createPost();


    @FormUrlEncoded
    @POST("posts")
    Call<News> createPostWithParams(@Field("title") String title,
                                        @Field("body") String body,
                                        @Field("userId") int userId);


    @Headers({"Cache-Control: max-age=640000",
            "User-Agent: Retrofit-Sample-App"})
    @POST("posts")
    Call<News> createPostWithJson(@Body News post, @Header("Authorization") String auth);
}